FROM openjdk:8
ADD target/docker-spring-boot-sample.jar docker-spring-boot-sample.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-sample.jar"]