package com.thisara.bean;

import java.util.ArrayList;
import java.util.List;

public class DeviceManager {

	enum MyDevices{
		PHONE,
		CAMERA,
		COMPUTER,
		TV,
		WATCH,
		DVD
	}
	
	private static DeviceManager deviceManager;
	
	private List<Device> devices = new ArrayList<>();
	
	public List<Device> getDevices() {
	
		//List<String> myDevices = 
		MyDevices[] md= MyDevices.values();
                		
		Device device = null;
		int deviceSequence = 123;
		//String[] myDevices = myDevices;
		
		
		for(int x=0; x<5; x++) {
		
			device = new Device();
			
			device.setDeviceId(deviceSequence);
			device.setDeviceName(String.valueOf(md[x]));

			devices.add(device);

			deviceSequence ++;
		}
				
		return devices;
	}
	
	public static DeviceManager getInstance() {
		
		if(deviceManager == null) {
			deviceManager = new DeviceManager();
			return deviceManager;
		}else {
			return deviceManager;
		}		
	}
}
