package com.thisara.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thisara.bean.Device;
import com.thisara.bean.DeviceManager;

@Controller
public class DeviceController {

	public List<Device> devices = new ArrayList<Device>();
	
	@RequestMapping(method = RequestMethod.GET, value = "/devices")
	@ResponseBody
	public List<Device> getAllDevices(){
		
		return DeviceManager.getInstance().getDevices();

	}
	
}
